﻿// See https://aka.ms/new-console-template for more information
//JOIN TASK 
using ConsoleApp1.Model;

List<Product> products;


products = new List<Product>()
            {
                new Product(){ProductName="oneplus",ProductId=1},
                new Product(){ProductName="acer",ProductId=2},
                new Product(){ProductName="oppo",ProductId=3},
                new Product(){ProductName="vivo",ProductId=4}
                
            };

List<Shopping> shopping;

shopping = new List<Shopping>()
{
    new Shopping(){ ShoppingId =5,ProductId=1,Quantity=10},
    new Shopping(){ ShoppingId =6,ProductId=2,Quantity=5}
};

var res = (from p in products
           join s in shopping
           on p.ProductId equals s.ProductId
           select new
           {
               productid = p.ProductId,
               productname = p.ProductName,
               productQuantity=s.Quantity
           });
foreach(var item in res)
{
    Console.WriteLine(item);
}
